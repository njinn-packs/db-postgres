import psycopg2
from psycopg2.extensions import parse_dsn


class PostgresBase:
    postgresconnection = {}
    cnxn = None
    cursor = None

    def connect(self):
        con_options = {}
        dsn = self.postgresconnection.get("custom_connection")
        if dsn:
            con_options = parse_dsn(dsn)
            print("Establishing connection using custom connection string.")
        else:
            con_options["host"] = self.postgresconnection.get("host")
            con_options["dbname"] = self.postgresconnection.get("dbname")
            con_options["user"] = self.postgresconnection.get("user")
            con_options["password"] = self.postgresconnection.get("password")

            if not con_options["host"]:
                raise Exception("No database host provided.")

            print("Establishing connection using connection parameters:")
            print({k: v for k, v in con_options.items() if k not in ["password"]})
        print()

        self.cnxn = psycopg2.connect(**con_options)
        self.cursor = self.cnxn.cursor()

    def disconnect(self):
        self.cnxn.close()
        print("Connection closed")


class RunQuery(PostgresBase):
    query = ""

    def run(self):
        self.connect()
        try:
            with self.cnxn:
                print("Executing query.")
                result = None
                self.cursor.execute(self.query)

                try:
                    rows = self.cursor.fetchall()
                    print(f"{len(rows)} row(s) retrieved")
                    rows_list = [[col for col in row] for row in rows]
                    result = {"rows": rows_list}
                    return result
                except psycopg2.ProgrammingError:
                    print("Query didn't return a resultset")

                rows = self.cursor.rowcount
                if rows != -1:
                    print(f"{rows} row(s) affected")
                else:
                    print(f"Can't determine how many rows affected.")

                return result
        finally:
            self.disconnect()
